﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zoology
{
    class Program
    {
        public static string GetUserInput()
        {
            string userInput = Console.ReadLine().ToLower();
            return userInput;
        }

        public static List<Animal> collectionAnimals = new List<Animal>();
        public static List<Animal> collectionAnimalsPrint = new List<Animal>();


        private static void Fillist(List<Animal> list)
        {
            list.Add(new Cat("Alice", "Cat", 10.00, 70, "Birdman"));
            list.Add(new Bear("Kent", "Bear", 90.00, 0, "Black Bear"));
            list.Add(new Bear("Anita", "Bear", 90.00, 0, "Black Bear"));
            list.Add(new Cat("Pus", "Cat", 5.00, 75, "Burmese"));
            list.Add(new Bear("Lars", "Bear", 90.00, 0, "Black Bear"));
            list.Add(new Cat("Junior", "Cat", 7.00, 80, "Bengal"));
        }

        static void Main(string[] args)
        {

            /* 
            Refactor code, waiting to remove. 
            
            Fillist(collectionAnimals);

            IHiders bear = new Bear("Lars", "Bear", 90.00, 0, "Black Bear");
            bear.Hide();

            foreach (Animal animal in collectionAnimals)
            {
                animal.AnimalSound();
            }

            End of refactor.
            */




            // Adding animals to the list, either custom where the user inputs the required fields or a predetermined list which use the method Fillist();
            try
            {
                Console.WriteLine("Do you want to add the list, or add 6 Animals of your choosing? Please answer list or custom.");
                string checkString = GetUserInput();

                // Fill list with predefined objects.
                if (checkString == "list")
                {
                    Fillist(collectionAnimals);
                }

                // Adding six animals to the list, determined by the user.
                else if (checkString == "custom")
                {
                    for (int i = 0; i < 6; i++)
                    {
                        Console.WriteLine("Please type bear, to create a new bear or cat for a cat.");
                        string nextAnimal = GetUserInput();
                        if (nextAnimal == "bear")
                        {
                            Console.WriteLine("What's the bears name?");
                            string name = GetUserInput();
                            string type = "Bear";
                            Console.WriteLine("What's the bears weight?");

                            double.TryParse(GetUserInput(), out double weight);

                            Console.WriteLine("What's the bears stealth level?");
                            int.TryParse(GetUserInput(), out int stealth);

                            Console.WriteLine("What's the bears breed type?");
                            string breed = Console.ReadLine();
                            collectionAnimals.Add(new Bear(name, type, weight, stealth, breed));
                        }
                        else if (nextAnimal == "cat")
                        {
                            Console.WriteLine("What's the cats name?");
                            string name = GetUserInput();
                            string type = "Cat";

                            Console.WriteLine("What's the cats weight?");
                            double.TryParse(GetUserInput(), out double weight);

                            Console.WriteLine("What's the cats stealth level?");
                            int.TryParse(GetUserInput(), out int stealth);

                            Console.WriteLine("What's the cats breed type?");
                            string breed = Console.ReadLine();
                            collectionAnimals.Add(new Bear(name, type, weight, stealth, breed));
                        }

                    }
                }
            }


            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Printing list of animals created by user.
            foreach (var item in collectionAnimals)
            {
                Console.WriteLine($"{item.Name} {item.Type} {item.Weight} {item.Stealth}");
            }

            // Adding statement to close the program when we want.
            bool running = true;
            // Instansiate running program, while true will keep running program
            while (running)
            {
                // Order and filtering feature, using linq to return item.
                try
                {
                    Console.WriteLine("Welcome to the order and filtering feature.");
                    Console.WriteLine("Please type order or filter to specify.");
                    string checkString = GetUserInput();

                    // Returning list ordered by weight or name using linq.
                    if (checkString == "order")
                    {
                        Console.WriteLine("Do you want to order by weight or name? please type weight or name");
                        string orderBy = GetUserInput();
                        if (orderBy == "weight")
                        {
                            collectionAnimalsPrint = collectionAnimals.OrderBy(x => x.Weight).ToList();
                        }
                        else if (orderBy == "name")
                        {
                            collectionAnimalsPrint = collectionAnimals.OrderBy(x => x.Name).ToList();
                        }
                    }

                    // Returning list of items containing filtered items.
                    else if (checkString == "filter")
                    {
                        Console.WriteLine("Do you want to filter by name or type?");
                        string filterString = GetUserInput();
                        if (filterString == "name")
                        {
                            Console.WriteLine("What name do you want to filter by?");
                            string containString = GetUserInput();
                            collectionAnimalsPrint = collectionAnimals.Where(x => x.Name.ToLower().Contains(containString)).ToList();
                        }
                        else if (filterString == "type")
                        {
                            Console.WriteLine("What type do you want to find?");
                            string containString = GetUserInput();
                            collectionAnimalsPrint = collectionAnimals.Where(x => x.Type.ToLower().Contains(containString)).ToList();
                        }

                    }

                    // Printing the list after ordering and filtering.
                    foreach (var item in collectionAnimalsPrint)
                    {
                        Console.WriteLine($"{item.Name} {item.Type} {item.Weight} {item.Stealth}");
                    }


                    // Option to exit the program.
                    Console.WriteLine("Do you want to exit the program? yes or no");
                    string userInput = GetUserInput();
                    if (userInput == "yes")
                    {
                        running = false;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
    }
}
