﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Bear : Animal, IHiders
    {
        public string BearBreed { get; set; }
        public Bear(string name, string type, double weight, int stealth, string bearBreed) : base(name,type,weight,stealth)
            {
                this.BearBreed = bearBreed;
            }

        public static void Hibernate(Bear bear)
        {
            Console.WriteLine($"{bear.Name} went to sleep");
        }

        public override void AnimalSound()
        {
            Console.WriteLine("The bear growls");
        }

        public void Hide()
        {
            Console.WriteLine("The bear is hiding.");
        }

    }
}
