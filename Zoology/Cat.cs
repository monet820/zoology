﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Cat : Animal
    {
        public string CatBreed { get; set; }

        public Cat(string name, string type, double weight, int stealth, string catBreed) : base(name, type, weight, stealth)
        {
            this.CatBreed = catBreed;
        }

        public override void AnimalSound()
        {
            Console.WriteLine("The cat mews");
        }
    }
}