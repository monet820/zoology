﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public abstract class Animal
    {
        // Type of animal.
        public string Type { get; set; }
        // Name of animal.
        public string Name { get; set; }
        // Weight of animal.
        public double Weight { get; set; }
        // Level of stealth 1-100
        public int Stealth { get; set; }

        public Animal() 
        {
            this.Name = "General Animal";
        }

        public Animal(string name, string type, double weight, int stealth)
            {
            Name = name;
            Type = type;
            Weight = weight;
            Stealth = stealth;
            }
        public void Hunt()
        {
            Console.WriteLine($"{this.Name} went into stealth, stealth level is {this.Stealth}");
        }

        public abstract void AnimalSound();

        
    }
}
